
## Development with docker (local)

Make sure your env file is fueled up and ready to go (see "Environment configuration" section)

```bash
# install dependencies
$ npm install

# build container with database
$ docker-compose build

# run container with database
$ docker-compose up

# run app 
$ npm  start
```
